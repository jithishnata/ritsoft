
<?php
 include("includes/connection.php");
 require('fpdf.php');
 session_start();
 $id=$_SESSION["fid"];
 $scode=$_POST['code'];

 $qry=mysql_query("select name, deptname, phoneno from faculty_details where fid='$id'",$con);  
 while($d=mysql_fetch_array($qry)){
    $fname=$d["name"];
  	$dname=$d["deptname"];
  	$pno=$d["phoneno"];
  }


  $r=mysql_query("select subject_title, classid from subject_class where subjectid='$scode'",$con );
  while($d=mysql_fetch_array($r)){
    $sub=$d["subject_title"];
    $clsid=$d["classid"];
  }
   $tok=explode(" ", $sub);
   $str=" ";
   foreach ($tok as $value) {$str=$str.$value[0];}


  $r=mysql_query("select semid, deptname, courseid  from class_details where classid='$clsid'",$con );
  while($d=mysql_fetch_array($r)){
    $branch=$d["courseid"] .' '. $d["deptname"];
    $sem=$d["semid"];
  }

  $r=mysql_query("select designation from faculty_designation where fid='$id'",$con );
  while($d=mysql_fetch_array($r)){
    $desi=$d["designation"];
  }

  $r=mysql_query("select acd_year from academic_year where status=1",$con);
  $d=mysql_fetch_array($r);
  $yr=$d['acd_year'];


  $pdf = new FPDF('P','mm','A4');
  $pdf->AliasNbpages();
  $pdf->AddPage();
   $pdf->SetMargins(10,10,10);
   $pdf->SetFont('Times','B',18);
   $pdf->Text(38,20,'RAJIV GANDHI INSTITUTE OF TECHNOLOGY');

   $pdf->SetFont('Times','B',16);
   $pdf->Text(78,27,'(Govt. Engg. College)');
   $pdf->Text(64,36,'Velloor P.O., Kottayam-686 501');
   $pdf->Image('clg_logo.jpg',78,50,60,50);
   $pdf->Text(85,115,'Year : ');
   $pdf->Text(105,115,$yr);

   $pdf->SetFont('Times','B',14);
   $pdf->Text(75,140,'ATTENDANCE REGISTER');
   $pdf->Text(105,147,'&');
   $pdf->Text(75,154,'FACULTY COURSE DIARY');

   $pdf->SetFont('Times','B',13);
   $pdf->Text(20,190,'Branch');
   $pdf->Text(100,190,':');

   $pdf->Text(20,205,'Semester');
   $pdf->Text(100,205,':');


   $pdf->Text(20,220,'Subject');
   $pdf->Text(100,220,':');
   

   $pdf->Text(20,235,'Name of Staff Member');
   $pdf->Text(100,235,':');
   

   $pdf->Text(20,250,'Mob. No.');
   $pdf->Text(100,250,':');
   

   $pdf->Text(20,265,'Designation');
   $pdf->Text(100,265,':');
   

   $pdf->Text(20,280,'Department');
   $pdf->Text(100,280,':');
   

   $pdf->SetFont('Times','',13);
   $pdf->Text(105,190,$branch);
   $pdf->Text(105,205,$sem);
   $pdf->Text(105,220,$sub);
   $pdf->Text(105,235,$fname);
   $pdf->Text(105,250,$pno);
   $pdf->Text(105,265,$desi);
   $pdf->Text(105,280,$dname);


   $pdf->AddPage();
   $pdf->SetMargins(10,10,10);
   $pdf->SetFont('Times','B',14);
   $pdf->cell(0,25,'General Instructions',0,1,'C');

   $pdf->SetFont('Arial','',12);
   $pdf->Cell(30,10,'*',0,0,'R');
   $pdf->Cell(0,10,'Student performance should be evaluated solely on an academic basis.',0,1,'L');

   $pdf->Cell(30,10,'*',0,0,'R');
   $pdf->Cell(0,10,'Student evaluation should be fair, consistent, transparent and accountable.',0,1,'L');

   $pdf->Cell(30,10,'*',0,0,'R');
   $pdf->Cell(0,10,"Evalua
    tion of student's performance should be disclosed to the students.",0,1,'L');
   $pdf->Ln();

   $pdf->Cell(10,7,'1.',0,0);
   $pdf->MultiCell(0,7,'Keep the Course Diary up to date by clearly indicating the subject coverage and students attendance on the relevant pages.',0);
   $pdf->Ln(5);

   $pdf->Cell(10,7,'2.',0,0);
   $pdf->MultiCell(0,7,'Paste the syllabus in the relevant page.');
   $pdf->Ln(5);

   $pdf->Cell(10,7,'3.',0,0);
   $pdf->MultiCell(0,7,'Write/paste the course plan in the relevant page.');
    $pdf->Ln(5);

   $pdf->Cell(10,7,'4.',0,0);
   $pdf->MultiCell(0,7,'Events in a semester such as Series Test days, Cultural/Celebration days, days for extra/co-curricular activities etc. may be indicated in the year calendar.');
    $pdf->Ln(5);

   $pdf->Cell(10,7,'5.',0,0);
   $pdf->MultiCell(0,7,'Assignment details may be written in the course diary or may be filled in the course file.');
    $pdf->Ln(5);

   $pdf->Cell(10,7,'6.',0,0);
   $pdf->MultiCell(0,7,'Show complete split up of sessional marks in the page Particulars of Marks. Final sessional for each student should be equal to the sum of marks awarded for Assignments and Series Tests. ');
   $pdf->Ln(5);

   $pdf->Cell(10,7,'7.',0,0);
   $pdf->MultiCell(0,7,'All the entries in the course diary must be legibly written without overwriting and free of errors.');
    $pdf->Ln(5);

   $pdf->Cell(10,7,'8.',0,0);
   $pdf->MultiCell(0,7,'Do not count the marks of class tests along with the series test for computing sessional marks.');
    $pdf->Ln(5);

   $pdf->Cell(10,7,'9.',0,0);
   $pdf->MultiCell(0,7,'The staff member will be responsible for the safe custody of the Course Diary and (s)he should return it to the HOD at the end of the semester or earlier if (s)he leaves the department or discontinues the subject.');
    $pdf->Ln(5);

   $pdf->Cell(10,7,'10.',0,0);
   $pdf->MultiCell(0,7,'Follow university regulations for computing sessional marks.');
    $pdf->Ln(15);
   $pdf->cell(0,10,'PRINCIPAL ',0,1,'R');

   $sq=mysql_query("select * from schedule_work where subjectid='$scode' and fid='$id'");
   $row=mysql_fetch_array($sq);
   $s=$row['schedule'];
   $sh=explode(",", $s);
   $pdf->AddPage();
   $pdf->SetFont('Times','B',12);
   $pdf->cell(0,50,'SCHEDULE OF WORK',0,1,'C');
   $pdf->SetFont('Arial','',12);
   $pdf->cell(40,20,'DAYS\HOURS',1,0,'C');
   $pdf->cell(25,20,'1',1,0,'C');
   $pdf->cell(25,20,'2',1,0,'C');
   $pdf->cell(25,20,'3',1,0,'C');
   $pdf->cell(25,20,'4',1,0,'C');
   $pdf->cell(25,20,'5',1,0,'C');
   $pdf->cell(25,20,'6',1,1,'C');

   $pdf->cell(40,20,'MON',1,0,'C');
   $pdf->SetFont('Arial','',10);
   if(in_array("mon1", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(75,80);
   if(in_array("mon2", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(100,80);
   if(in_array("mon3", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(125,80);
   if(in_array("mon4", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(150,80);
   if(in_array("mon5", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(175,80);
   if(in_array("mon6", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}

   $pdf->setXY(10,100);
   $pdf->SetFont('Arial','',12);
   $pdf->cell(40,20,'TUE',1,0,'C');
   $pdf->SetFont('Arial','',10);
   if(in_array("tue1", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(75,100);
   if(in_array("tue2", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(100,100);
   if(in_array("tue3", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(125,100);
   if(in_array("tue4", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(150,100);
   if(in_array("tue5", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(175,100);
   if(in_array("tue6", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);} 

   $pdf->setXY(10,120);
   $pdf->SetFont('Arial','',12);
   $pdf->cell(40,20,'WED',1,0,'C');
   $pdf->SetFont('Arial','',10);
   if(in_array("wed1", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(75,120);
   if(in_array("wed2", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(100,120);
   if(in_array("wed3", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(125,120);
   if(in_array("wed4", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(150,120);
   if(in_array("wed5", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(175,120);
   if(in_array("wed6", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}

   $pdf->setXY(10,140);
   $pdf->SetFont('Arial','',12);
   $pdf->cell(40,20,'THU',1,0,'C');
   $pdf->SetFont('Arial','',10);
   if(in_array("thu1", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(75,140);
   if(in_array("thu2", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(100,140);
   if(in_array("thu3", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(125,140);
   if(in_array("thu4", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(150,140);
   if(in_array("thu5", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(175,140);
   if(in_array("thu6", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}

   $pdf->SetFont('Arial','',12);
   $pdf->setXY(10,160);
   $pdf->cell(40,20,'FRI',1,0,'C');
   $pdf->SetFont('Arial','',10);
   if(in_array("fri1", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(75,160);
   if(in_array("fri2", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(100,160);
   if(in_array("fri3", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(125,160);
   if(in_array("fri4", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(150,160);
   if(in_array("fri5", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(175,160);
   if(in_array("fri6", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}

   $pdf->SetFont('Arial','',12);
   $pdf->setXY(10,180);
   $pdf->cell(40,20,'SAT',1,0,'C');
   $pdf->SetFont('Arial','',10);
   if(in_array("sat1", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(75,180);
   if(in_array("sat2", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(100,180);
   if(in_array("sat3", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(125,180);
   if(in_array("sat4", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(150,180);
   if(in_array("sat5", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}
   $pdf->setXY(175,180);
   if(in_array("sat6", $sh)){ $pdf->MultiCell(25,10,$scode.''.$str,1,'C',false); }
   else{$pdf->Cell(25,20,'',1,0);}


   $sql=mysql_query("select * from course_plan where subjectid='$scode'order by date");
   $pdf->AddPage();
   $pdf->SetFont('Times','B',12);
   $pdf->cell(0,20,'COURSE PLAN',0,1,'C');
   $pdf->SetFont('Arial','',13);
   $pdf->cell(20,20,'No.',1,0,'C');
   $pdf->cell(34,20,'Date & Day',1,0,'C');
   $pdf->cell(15,20,'Hr',1,0,'C');
   $pdf->cell(125,20,'Topics to be Covered',1,1,'C');
   $pdf->SetFont('Arial','',10);
   $i=1;
   $j=50;
   $k=50;
   while($res=mysql_fetch_assoc($sql)){
    $hr=$res["hour"];
    $topic=$res['topic'];
    $date=$res['date'];
    $dd = strtotime($date);
    $day = date('D', $dd);
    $pdf->setXY(10,$j);
    $pdf->cell(20,10,$i,1,0,'C');
    $pdf->cell(34,10,$date.' '.$day,1,0,'C');
    $pdf->cell(15,10,$hr,1,0,'C');

    $pdf->cell(125,10,$topic,1,1,'C');
    $j=$j+10;
    if($j>270){
     $pdf->AddPage();
      $j=10;
    }
    $i++;
    
  }



  $sql=mysql_query("select * from subject_coverage where subjectid='$scode'order by date");
   $pdf->AddPage();
   $pdf->SetFont('Times','B',12);
   $pdf->cell(0,20,'SUBJECT COVERAGE',0,1,'C');
   $pdf->SetFont('Arial','',13);
   $pdf->cell(20,20,'No.',1,0,'C');
   $pdf->cell(34,20,'Date & Day',1,0,'C');
   $pdf->cell(15,20,'Hr',1,0,'C');
   $pdf->cell(125,20,'Topics Covered',1,1,'C');
   $pdf->SetFont('Arial','',12);
   $i=1;
   $j=50;
   $k=50;
   while($res=mysql_fetch_assoc($sql)){
    $hr=$res["hour"];
    $topic=$res['topic'];
    $date=$res['date'];
    $dd = strtotime($date);
    $day = date('D', $dd);
    $pdf->setXY(10,$j);
    $pdf->cell(20,10,$i,1,0,'C');
    $pdf->cell(34,10,$date.' '.$day,1,0,'C');
    $pdf->cell(15,10,$hr,1,0,'C');
    $pdf->cell(125,10,$topic,1,1,'C');
    $j=$j+10;
    if($j>270){
     $pdf->AddPage();
      $j=10;
    }
    $i++;
    
  }


  $pdf->AddPage();
  $pdf->SetFont('Times','B',12);
  $pdf->cell(0,15,'ATTENDANCE REGISTER',0,1,'C');
  $pdf->cell(10,10,'Month : ',0,1);
  $pdf->cell(15,15,'Roll No.',1,0,'C');
  $pdf->cell(40,15,'Name',1,0,'C');
  $pdf->cell(15,5,'Month',1,1,'C');
  $pdf->setX(65);
  $pdf->cell(15,5,'Date',1,1,'C');
  $pdf->setX(65);
  $pdf->cell(15,5,'Period',1,'C');


  $c=mysql_query("SELECT distinct date,hour FROM attendance WHERE classid='".$clsid."' and subjectid='".$scode."' order by date,hour;");
  $harray=array();
  $x=80;
  while($res=mysql_fetch_array($c))
  {
    $harray[$res[0].";".$res[1]]=$res[1];  
  }
  class attendance
  {
    public $name="";
    public $rollno="";
    public $att=array();
    function __construct($rollno,$name)
    {
      $this->name=$name;
      $this->rollno=$rollno;
    }
    function setAttendance($datehour,$status)
    {
      $this->att[$datehour]=$status;
    }
    function getAttendance($date)
    {
      if(array_key_exists($date,$this->att))
      {
        $value=$this->att[$date];
        if($value=="P")
        {
          return ("P");
        }
        if($value=="A")
        {
          return ("A");
        }
      }
      return ("-");
    }
  }
  $qr="SELECT C.rollno,S.admissionno as stdid,S.name FROM stud_details as S left join current_class as C on S.admissionno=C.studid where C.classid='".$clsid."' order by C.rollno;";
  $cy=mysql_query($qr);
  $studlist=array();
  while($resb=mysql_fetch_assoc($cy))
  {
    $studlist[$resb['stdid']]=new attendance($resb['rollno'],$resb['name']);
  }
  foreach($harray as $xdate => $hr)
  {
    $xdate=explode(";",$xdate)[0];
    $qr="SELECT hour,studid,status FROM attendance where classid='".$clsid."' and date='".$xdate."' order by studid,hour asc;";
    $cz=mysql_query($qr);
    while($resc=mysql_fetch_assoc($cz))
    {
      if(array_key_exists($resc['studid'],$studlist))
      $studlist[$resc['studid']]->setAttendance($xdate.":".$hr,$resc['status']);
    }
  }
  $y=50;
  $pdf->SetFont('Arial','',10);
  foreach($studlist as $id => $student)
  {

    $pdf->setXY(10,$y);
    $pdf->cell(15,4,$student->rollno,1,0,'C');
    $pdf->cell(55,4,$student->name,1,0);
    $y+=4;
  }
    foreach($harray as $date => $hour)
    {
      if($x > 200)
      {
        $pdf->AddPage();
        $pdf->SetFont('Times','B',12);
        $pdf->cell(0,15,'ATTENDANCE REGISTER',0,1,'C');
        $pdf->cell(10,10,'Month : ',0,1);
        $pdf->cell(15,15,'Roll No.',1,0,'C');
        $pdf->cell(40,15,'Name',1,0,'C');
        $pdf->cell(15,5,'Month',1,1,'C');
        $pdf->setX(65);
        $pdf->cell(15,5,'Date',1,1,'C');
        $pdf->setX(65);
        $pdf->cell(15,5,'Period',1,'C');
        $x=80;
        $y=50;
        $pdf->SetFont('Arial','',10);
        foreach($studlist as $id => $student)
        {

          $pdf->setXY(10,$y);
          $pdf->cell(15,4,$student->rollno,1,0,'C');
          $pdf->cell(55,4,$student->name,1,0);
          $y+=4;
        }
      }
       $date=explode(";",$date)[0];
       $fdate=explode("-", $date);
       $m=$fdate[1];
       $d=$fdate[2];
       $pdf->setXY($x,35);
       $pdf->cell(7,5,$m,1,1,'C');
       $pdf->setX($x);
       $pdf->cell(7,5,$d,1,1,'C');
       $pdf->setX($x);
       $pdf->cell(7,5,$hour,1,1,'C');
       foreach($studlist as $id => $student)
       {
       $sts=$student->getAttendance($date.":".$hour);
       $pdf->setX($x);
       $pdf->cell(7,4,$sts,1,1,'C');
       }
       $x+=7;

    }
    
  
  

   $pdf->Output();
?>